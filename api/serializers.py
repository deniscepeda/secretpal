from rest_framework import serializers
from .models import Pal, SecretPalRelation


class PalSerializer(serializers.ModelSerializer):
    """Serializer to map the Pal Model instance into DB"""

    class Meta:
        model = Pal
        fields = ('id', 'name', 'department', 'date_created')
        read_only_fields = ('date_created', )


class SecretPalRelationSerializer(serializers.ModelSerializer):
    """Serializer to map the SecretPalRelation Model instance into DB"""
    giver = serializers.PrimaryKeyRelatedField(read_only='True')
    receiver = serializers.PrimaryKeyRelatedField(read_only='True')

    class Meta:
        model = SecretPalRelation
        fields = ('giver_id', 'receiver_id')
