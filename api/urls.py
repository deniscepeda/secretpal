from django.conf.urls import url
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreatePalView, PalDetailsView, RaffleView, RaffleResultView

urlpatterns = {
    url(r'^pals/$', CreatePalView.as_view(), name="pal"),
    url(r'^pals/(?P<pk>[0-9]+)/$', PalDetailsView.as_view(), name="palDetails"),
    url(r'^raffle/$', RaffleView.as_view(), name="raffle"),
    url(r'^raffle/(?P<pk>[0-9]+)/$', RaffleResultView.as_view(), name="raffleResult"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
