# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import random

from django.core import serializers
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import generics, status
from rest_framework.exceptions import APIException
from rest_framework.response import Response
from rest_framework.views import APIView

from .serializers import PalSerializer
from .models import Pal, SecretPalRelation


######################
# PAL MANAGING VIEWS #
######################
class CreatePalView(generics.ListCreateAPIView):
    """This class defines the "Create Pal" behavior when a POST is received.
    It also allows a GET query that returns all the Pals"""
    queryset = Pal.objects.all()
    serializer_class = PalSerializer

    def perform_create(self, serializer):
        """It saves the data sent in the POST, by creating a new Pal.
        It will only work if the raffle has not been done yet"""
        if SecretPalRelation.objects.count() > 0:
            raise APIException(detail='The raffle has already been made')

        serializer.save()


class PalDetailsView(generics.RetrieveUpdateDestroyAPIView):
    """This class handles the http individual requests (GET, PUT and DELETE)"""
    queryset = Pal.objects.all()
    serializer_class = PalSerializer


########################
# RAFFLE RELATED VIEWS #
########################
class RaffleView(APIView):
    """This view's POST method shall be called when all the Pals are already created.
    It will do the raffle, assigning every Pal (giver) a Secret Pal (receiver).
    After this moment, no more Pals will be allowed in the system.
    This POST method can only be called once in the program's lifetime"""

    def post(self, request):
        # Check that the raffle has not been done yet
        if len(SecretPalRelation.objects.all()) > 0:
            return Response(status=status.HTTP_403_FORBIDDEN, data='The raffle has already been made')

        # Get all the Pals
        pals = Pal.objects.all()
        if len(pals) < 2:
            return Response(status=status.HTTP_403_FORBIDDEN, data='There are not enough Pals')

        pals_dict = {}
        for pal in pals:
            pals_dict[pal.pk] = pal

        # Do the raffle:
        # We shuffle the IDs and make a circular list.
        # Every Pal will give to the next Pal in the list.
        # That way there won't be loops, and the presents' give-away will flow better
        pal_ids = pals_dict.keys()
        random.shuffle(pal_ids)

        # Store the results
        for i in range(0, len(pal_ids) - 1):
            p1 = pals_dict[pal_ids[i]]
            p2 = pals_dict[pal_ids[i+1]]
            relation = SecretPalRelation(giver=p1, receiver=p2)
            relation.save()

        # The last one has special treatment
        p1 = pals_dict[pal_ids[len(pal_ids) - 1]]
        p2 = pals_dict[pal_ids[0]]
        relation = SecretPalRelation(giver=p1, receiver=p2)
        relation.save()

        # Return that everything went OK
        return Response(status=status.HTTP_200_OK)


class RaffleResultView(APIView):
    """This class allows a Pal to check who is his Secret Pal
    It only works if the Raffle has already been done"""

    def get(self, request, pk):
        # Get the Pal
        try:
            pal = Pal.objects.get(pk=int(pk))
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND, data='There is no Pal with the given pk')

        # Get his Secret Pal
        try:
            relation = SecretPalRelation.objects.get(giver=pal)
        except ObjectDoesNotExist:
            return Response(status=status.HTTP_403_FORBIDDEN, data='The raffle has not been done yet')

        # Return it
        json_secret_pal = serializers.serialize('json', [relation.receiver, ])
        return Response(status=status.HTTP_200_OK, data=json_secret_pal)
