# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.test import TestCase

from rest_framework.test import APIClient
from rest_framework import status
from django.core.urlresolvers import reverse

from .models import Pal


class ModelTestCase(TestCase):
    """Test suite for the Pal model"""

    def setUp(self):
        pass

    def test_model_01_create_pal(self):
        old_count = Pal.objects.count()

        my_pal = Pal(name='Ernesto Mate', department='IT')
        my_pal.save()

        new_count = Pal.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_02_create_pal(self):
        old_count = Pal.objects.count()

        my_pal = Pal(name='Karl Toffel')
        my_pal.save()

        new_count = Pal.objects.count()
        self.assertNotEqual(old_count, new_count)


class PalCrudTestCase(TestCase):
    """Test suite for the API views that implement the Pal CRUD"""

    def setUp(self):
        self.client = APIClient()

        # Create a demo pal
        pal_data = {
            'name': 'Cristina Menaza',
            'department': 'Management'
        }

        response = self.client.post(
            reverse('pal'),
            pal_data,
            format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.demo_pal_id = response.json()['id']

    # Create
    # ------
    def test_crud_01_create_pal(self):
        pal_data = {
            'name': 'Aitor Tilla',
            'department': 'Sales'
        }
        response = self.client.post(
            reverse('pal'),
            pal_data,
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_crud_02_create_pal(self):
        pal_data = {
            'name': 'Blanca Nieves',
        }
        response = self.client.post(
            reverse('pal'),
            pal_data,
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_crud_03_create_pal(self):
        pal_data = {
            'name': 'Cristina Menaza',  # Repeated name
            'department': 'Sales'
        }
        response = self.client.post(
            reverse('pal'),
            pal_data,
            format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    # Retrieve all
    # ------------
    def test_crud_11_get_all(self):
        response = self.client.get(
            reverse('pal'),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.json()), 1)

    # Retrieve one
    # ------------
    def test_crud_21_get_pal(self):
        response = self.client.get(
            reverse('palDetails', kwargs={'pk': self.demo_pal_id}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['id'], self.demo_pal_id)

    def test_crud_22_get_pal(self):
        response = self.client.get(
            reverse('palDetails', kwargs={'pk': 0}),  # Wrong id
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # Update
    # ------
    def test_crud_31_update_pal(self):
        fields = {
            'name': 'Kristina Menaza',
        }
        response = self.client.put(
            reverse('palDetails', kwargs={'pk': self.demo_pal_id}),
            fields,
            format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['name'], 'Kristina Menaza')

    def test_crud_32_update_pal(self):
        fields = {
            'name': 'Kristina Menaza',
            'department': 'Human Resources',
        }
        response = self.client.put(
            reverse('palDetails', kwargs={'pk': self.demo_pal_id}),
            fields,
            format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()['name'], 'Kristina Menaza')
        self.assertEqual(response.json()['department'], 'Human Resources')

    def test_crud_33_update_pal(self):
        fields = {
            'department': 'Human Resources',
        }
        response = self.client.put(
            reverse('palDetails', kwargs={'pk': 0}),  # Wrong id
            fields,
            format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # Delete
    # ------
    def test_crud_41_delete_pal(self):
        response = self.client.delete(
            reverse('palDetails', kwargs={'pk': self.demo_pal_id}),
            format='json')

        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_crud_42_delete_pal(self):
        response = self.client.delete(
            reverse('palDetails', kwargs={'pk': 0}),  # Wrong id
            format='json')

        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)


class RaffleApiTestCase(TestCase):
    """Test suite for the API views associated to the Raffle"""

    def setUp(self):
        self.client = APIClient()

    # Do the raffle
    # -------------
    def test_raffle_01_do_it(self):
        # Try to raffle without users
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_raffle_02_do_it(self):
        # Create some Pals
        self.create_demo_friends()

        # Do the raffle
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_raffle_03_do_it(self):
        # Create some Pals
        self.create_demo_friends()

        # Do the raffle
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Try to do the raffle again
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    # Get the raffle results
    # ----------------------
    def test_raffle_11_get_result(self):
        # Create a Pal
        demo_pal_id = self.create_demo_pal()

        # Try to get his Secret Pal
        response = self.client.get(
            reverse('raffleResult', kwargs={'pk': demo_pal_id}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_raffle_12_get_result(self):
        # Create a Pal and some friends
        self.create_demo_friends()
        demo_pal_id = self.create_demo_pal()

        # Do the raffle
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Get his Secret Pal
        response = self.client.get(
            reverse('raffleResult', kwargs={'pk': demo_pal_id}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        secret_pal = json.loads(response.json())
        self.assertTrue('name' in secret_pal[0]['fields'])
        self.assertTrue('department' in secret_pal[0]['fields'])

    def test_raffle_13_get_result(self):
        # Create some Pals
        self.create_demo_friends()

        # Do the raffle
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Try to get the result of an invented Pal
        response = self.client.get(
            reverse('raffleResult', kwargs={'pk': 0}),
            format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    # BONUS: Create Pal after the Raffle
    # ----------------------------------
    def test_raffle_21_create_after_raffle(self):
        # Create some friends
        self.create_demo_friends()

        # Do the raffle
        response = self.client.post(reverse('raffle'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Try to create a new Pal
        response = self.client.post(
            reverse('pal'),
            {'name': 'Alberto Rijas', 'department': 'maintenance'},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    # Auxiliary methods
    # -----------------
    def create_demo_pal(self):
        response = self.client.post(
            reverse('pal'),
            {'name': 'Alberto Rijas', 'department': 'maintenance'},
            format="json")

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        return response.json()['id']

    def create_demo_friends(self):
        pals_data = [
            {'name': 'Cristina Menaza', 'department': 'Management'},
            {'name': 'Aitor Tilla', 'department': 'Sales'},
            {'name': 'Ernesto Mate', 'department': 'IT'},
            {'name': 'Karl Toffel'},
        ]
        for pal_data in pals_data:
            response = self.client.post(
                reverse('pal'),
                pal_data,
                format="json")
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)
