# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Pal(models.Model):
    """A Pal is a person who participates in the activity"""
    name = models.CharField(max_length=200, blank=False, unique=True)
    department = models.CharField(max_length=100, blank=True)
    date_created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s (%s)' % (self.name, self.department)


class SecretPalRelation(models.Model):
    """This model stores the relation between the giver and the receiver (both Pals)"""
    giver = models.ForeignKey(Pal, related_name='giver_id')
    receiver = models.ForeignKey(Pal, related_name='receiver_id')

    def __str__(self):
        return '%s gives to %s' % (self.giver.name, self.receiver.name)
